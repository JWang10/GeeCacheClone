package main

import (
	"flag"
	"fmt"
	"geecache"
	"log"
	"net/http"
)

/*
$ curl http://localhost:7000/api\?key\=Tom
630%
$ curl http://localhost:7000/api\?key\=Jack
589%
---logs
2022/07/13 17:47:40 cache server is running at  http://localhost:8000
2022/07/13 17:47:40 cache server is running at  http://localhost:8002
2022/07/13 17:47:40 cache server is running at  http://localhost:8001
2022/07/13 17:47:40 api server is running at http://localhost:7000
>>> start test
2022/07/13 17:47:41 [Server http://localhost:8002] Pick peer: http://localhost:8000
2022/07/13 17:47:41 [Server http://localhost:8000] GET /_cache/score/Tom
2022/07/13 17:47:41 [testDB] search key Tom
6306306302022/07/13 17:47:59 [Server http://localhost:8002] Pick peer: http://localhost:8000
2022/07/13 17:47:59 [Server http://localhost:8000] GET /_cache/score/Tom
2022/07/13 17:47:59 [Cache] hit
2022/07/13 17:48:05 [Server http://localhost:8002] Pick peer: http://localhost:8001
2022/07/13 17:48:05 [Server http://localhost:8001] GET /_cache/score/Jack
2022/07/13 17:48:05 [testDB] search key Jack
*/

var db = map[string]string{
	"Tom":  "630",
	"Jack": "589",
	"Sam":  "567",
}

func main() {
	var port int
	var api bool
	flag.IntVar(&port, "port", 8000, "Cache server port")
	flag.BoolVar(&api, "api", false, "Start a api server?")
	flag.Parse()

	apiAddr := "http://localhost:7000"
	addrMap := map[int]string{
		8000: "http://localhost:8000",
		8001: "http://localhost:8001",
		8002: "http://localhost:8002",
	}

	var addrs []string
	for _, val := range addrMap {
		addrs = append(addrs, val)
	}
	cache := creatGroup()
	if api {
		go StartApiServer(apiAddr, cache)
	}

	StartCacheServer(addrMap[port], []string(addrs), cache)
}

func creatGroup() *geecache.Group {
	return geecache.NewGroup("score", 2<<10, geecache.GetterFunc(
		func(key string) ([]byte, error) {
			log.Println("[testDB] search key", key)
			if val, ok := db[key]; ok {
				return []byte(val), nil
			}
			return nil, fmt.Errorf("%s not exist", key)
		}))
}

func StartCacheServer(addr string, addrs []string, cache *geecache.Group) {
	peers := geecache.NewHttpPool(addr)
	peers.Set(addrs...)
	cache.RegisterPeers(peers)
	log.Println("cache server is running at ", addr)
	log.Fatalln(http.ListenAndServe(addr[7:], peers))
}

func StartApiServer(addr string, cache *geecache.Group) {

	http.HandleFunc("/api", func(w http.ResponseWriter, r *http.Request) {
		key := r.URL.Query().Get("key")
		view, err := cache.Get(key)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/octet-stream")
		_, err = w.Write(view.ByteSlice())
		if err != nil {
			log.Println("write wrong", err)
			return
		}
	})
	log.Println("api server is running at", addr)
	log.Fatal(http.ListenAndServe(addr[7:], nil))
}
