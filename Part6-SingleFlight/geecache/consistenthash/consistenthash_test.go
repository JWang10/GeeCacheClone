package consistenthash

import (
	"log"
	"strconv"
	"testing"
)

func TestHashing(t *testing.T) {
	hash := NewHash(3, func(key []byte) uint32 {
		i, _ := strconv.Atoi(string(key))
		return uint32(i)
	})
	// Given the above hash function, this will give replicas with "hashes":
	// real    key: 2, 4, 6
	// virtual key: 2, 12, 22, 4, 14, 24, 6, 16, 26
	hash.Add("6", "4", "2")

	testCases := map[string]string{
		"2":  "2",
		"11": "2",
		"23": "4",
		"27": "2",
	}

	for k, v := range testCases {
		log.Println(k, v)
		if hash.Get(k) != v {
			t.Errorf("Asking for %s, should have yielded %s", k, v)
		}
	}

	// add 8, 18, 28
	hash.Add("8")
	// 27 should map to 8 now
	testCases["27"] = "8"

	for k, v := range testCases {
		if hash.Get(k) != v {
			t.Errorf("Asking for %s, should have yielded %s", k, v)
		}
	}

}
