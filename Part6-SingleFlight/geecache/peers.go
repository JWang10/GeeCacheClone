package geecache

// PeerGetter is an interface that be implemented by a peer/client
type PeerGetter interface {
	Get(group string, ket string) ([]byte, error)
}

// PeerPicker is an interface that be implemented to locate the peer that has a specific key
type PeerPicker interface {
	PickPeer(key string) (peer PeerGetter, ok bool)
}
