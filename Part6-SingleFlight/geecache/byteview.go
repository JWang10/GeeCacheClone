package geecache

// ByteView hold an immutable view of bytes
type ByteView struct {
	b []byte
}

// Len returns the length of the view
func (v ByteView) Len() int {
	return len(v.b)
}

// ByteSlice returns a copy of data as a byte slice
func (v ByteView) ByteSlice() []byte { // 用途防止緩存被外部修改
	return cloneBytes(v.b)
}

func cloneBytes(b []byte) []byte {
	var newB = make([]byte, len(b))
	copy(newB, b)
	return newB
}

// String returns data as a string, making a copy if necessary
func (v ByteView) String() string {
	return string(v.b)
}
