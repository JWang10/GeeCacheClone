#!/bin/zsh
# trap for removing temp files after closing shell
trap "rm server; kill 0" EXIT

go build -o server

./server -port=8000 &
./server -port=8001 &
./server -port=8002 -api=true &

sleep 2
echo ">>> start test"
curl "http://localhost:7000/api?key=Tom" &
curl "http://localhost:7000/api?key=Tom" &
curl "http://localhost:7000/api?key=Tom" &

wait