# Gee Cache Clone

a distributed cache system clone from scratch from geektutu

## Part1 - LRU

此處緩存全部會存在RAM（內存、記憶體）中，RAM是有限的，因此需要有機制在佔用內存超過設置緩存，進行無用緩存清除。
常用的緩存淘汰算法有以下幾個

- FIFO：First In First Out
  - 最早添加的紀錄不被使用的可能性大於剛添加的紀錄
  - 其機制可以透過`Queue`實現
  - 但真實場景中，部分數據雖然最早被加入，但是也最常被訪問，在此處造成缺點為頻繁刪除加入，導致`緩存命中率低`
- LFU：Least Frequency Used
  - 最少使用的紀錄，也就是淘汰緩存中訪問率最低的紀錄
  - 其機制需要維護一個`按照訪問次數`的陣列
    - 每次訪問次數加1，陣列重新排序，淘汰時選擇訪問次數最少的
  - 緩存命中率較高，但缺點為維護每個紀錄的訪問次數，對內存消耗是較高的
  - LFU算法受歷史數據影響較大
    - 例如某個歷史數據訪問次數高，但在某時間點後卻沒訪問了，但因歷史訪問過高，而不會被刪除
- LRU：Least Recently Used
  - 最近最少使用的
  - 相對前面兩種，LRU算法則是相對平衡的一種淘汰算法
    - LRU認為如果數據最近被訪問，則將來被訪問的機率也相對較高
  - 其機制維護一個列表，如果某條紀錄被訪問，則把紀錄移到隊尾，那隊首則是最近最少訪問的數據，直接淘汰即可

此部分
- 實現LRU緩存淘汰算法
- 加入單元測試

## Part2 - Concurrency

多協程(`goroutine`)同時讀寫同一個變量時，在併發較高情況下，會發生衝突。
互斥鎖的用途，為確保一次只有一個`goroutine`可以訪問變量，避免衝突發生

- sync.Mutex
  - 一種互斥鎖，可以由不同協程加鎖和解鎖
  - e.g. 當一個協程拿到互斥鎖權限後，其他請求鎖的協程會卡在`Lock()`調用，直到原協程調用`Unlock()`釋放鎖

- 支持併發讀寫
  - 使用`sync.Mutex`封裝`LRU`幾個方法，達成併發讀寫

```go
  var m sync.Mutex
  var set = make(map[int]bool, 0)
  
  func printOnce(num int) {
    m.Lock()             // 如果沒有加入互斥鎖，print次數可能是4、10次不定，有時候可能panic
	defer m.Unlock() // 加入鎖以後，相同次數的數字只會出現一次
    if _, exist := set[num]; !exist {
    fmt.Println(num)
    }
    set[num] = true
  }
  
  func main() {
  for i := 0; i < 10; i++ {
  go printOnce(100)
  }
  time.Sleep(time.Second)
  }
```
- Group 
  - 以下流程示意圖
```mermaid
graph TD
A[接收key] --> B{檢查是否有緩存}
B --> |是|B1[返回緩存值-1]
B --> |否|B2[是否從遠端節點獲取數據]
B2 --> |是|C[與遠端節點交互] 
C --> C1[返回緩存值-2] 
C --> |否|E[調用callback 獲取回傳值並加入到緩存]
E --> E1[返回緩存值-3]
```



此部分

- 介紹`sync.Mutex`互斥鎖使用，且實現`LRU`緩存的併發控制
- 實現`GeeCacheClone`核心數據結構`Group`
  - 緩存不存在，則調用`callback`獲取數據


---
單機併發緩存

對應上述流程圖

- 流程(1)：從`mainCache`查找緩存
- 流程(3)：緩存不存在，則調用`load`讀取數據，目前數據是本地調用`getLocally`
  調用用戶`callback`函數`g.getter.Get()`獲取數據，並將數據透過`populateCache`加到緩存`mainCache`中

```go
func (g *Group) Get(key string) (ByteView, error) {
	if key == "" {
		return ByteView{}, fmt.Errorf("key is required")
	}

	if v, ok := g.mainCache.get(key); ok {
		log.Println("[GeeCache] hit")
		return v, nil
	}
	return g.load(key) // 根據場景選擇需要獲取的來源：分佈式場景會調用 getFromPeer 從其他節點獲取數據
}

func (g *Group) load(key string) (ByteView, error) {
    return g.getLocally(key)
}

func (g *Group) getLocally(key string) (ByteView, error) {
  bytes, err := g.getter.Get(key)
  if err != nil {
    return ByteView{}, err
  }
  
  value := ByteView{b: cloneBytes(bytes)}
  g.populateCache(key, value)
  return value, nil
}

func (g *Group) populateCache(key string, value ByteView) {
    g.mainCache.add(key, value)
}
```

## Part 3 - Http Server

簡易`http server`啟動代碼

```go
import (
  "log"
	"net/http"
)
func main(){
  http.HandleFunc("/",(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Hello World!"))
  })
  err := http.ListenAndServe(":8080",nil)
  if err != nil {
    log.Fatal("ListenAndServe: ", err)
  }
}
```

標準庫中，`http.Handler`接口定義

```go
package http

type Handler interface {
    ServeHTTP(w ResponseWriter, r *Request)
}
```

此part稍微簡易些

- 使用Go標準庫`http`構建`Http Server`
- 測試`http server`的API

### GeeCache Http Server

建立一個結構體`HttpPool`，只有2個參數：

- self：紀錄自己位址，包含host、port
- basePath：節點間通訊地址的前綴判斷，默認為`_cache`。通常透過這種方式區分其他服務

```go
// HttpPool implements PeerPicker for a pool of HTTP peers.
type HttpPool struct {
	self     string
	basePath string
}

```

核心的`ServeHTTP`方法

- `ServeHTTP`的實現邏輯為，判斷訪問路徑前綴是否為`basePath`，不是則返回錯誤
- 設定訪問路徑格式為 `/<basepath>/<groupname>/<key>`，透過groupname得到group，再使用`group.Get(key)`拿去緩存資料
- 最後一步，使用 `w.Write()`把緩存值作為`httpResponse`的`body`返回

測試如下

```go
$ curl http://localhost:8080/_cache/scor
bad request
$ curl http://localhost:8080/_cache/score/3
3 not exist
$ curl http://localhost:8080/_cache/score/Tom
630

--- logs
2022/07/08 17:31:10 [Server :8080] GET /_cache/scor
2022/07/08 17:31:13 [Server :8080] GET /_cache/score/3
2022/07/08 17:31:13 [TestDB] search key:  3
2022/07/08 17:31:23 [Server :8080] GET /_cache/score/Tom
2022/07/08 17:31:23 [TestDB] search key:  Tom

```

## Part 4 - Consistent Hashing

一致性hash算法是此project從單節點走向分布式節點的重要環節。

在分佈式緩存中，如果從隨機節點中緩存的話，那遇到沒緩存的節點，勢必要從數據後端獲取數據，會花很大的成本且耗時的。這樣做，一是緩存效率低，二是每個節點都儲存相同的數據，浪費了大量的儲存空間。

一般對於key，我們會採用`hash值計算+取餘數(mod)`方式，來保證都存取到相同的節點。

雖然hash值解決了緩存性能問題，但這部分在節點數量改變情況下，原本計算的hash值皆全部失效。節點在接受請求時，會重新去獲取數據，此場景容易發生`緩存雪崩`。

因此一致性hash算法（環狀）則是解決此問題而誕生的。

一致性hash算法將key映射到2^32的空間中，頭尾相連，形成一個閉環。

- 下方圖片ref：geektutu

![consistency_hash.png](assets/consistency_hash.png)

原理部分簡單敘述為，

- 計算key的hash值，加入環中
- 在環上順時針找到第一個節點，為對應存放的節點
- 如上圖，在新增一個節點peer8，這時候只需把`key27`對映從peer2改成peer8即可。只需重新對應該節點的一小部分數據，而不用重新映射全部節點。

### 數據傾斜問題

如果節點過少，容易發生key傾斜。如上圖左邊為例，如果大多都映射到peer2，而導致另一半環是空的，會造成緩存節點負載不均情況。

針對此問題，引入了`虛擬節點`的概念，一個真實節點對應多個虛擬節點關係。解決了節點少導致數據傾斜問題，只需要一個`map`維護真實節點和虛擬節點的映射關係即可。

原理為

- 計算虛擬節點的hash值，加入環中
- 計算key的hash值，在環上順時針找到對應的虛擬節點。
  - e.g. peer1-1對應 peer1

此部分

- 實現一致性hash，並加入測試例子

## Part 5 - Distributed Cache

在前面部分，敘述了cache流程，之前也實現了流程1、3部分，此部分將實現流程2，從遠端節點獲取緩存值

我們將先把流程2細化

```mermaid
graph TD
	A[使用一致性hash算法選擇節點] --> |是| B[http client訪問遠端節點] 
	A --> |否|E[本地節點處理]
	B --> C{成功}
	C --> |是|D[Server返回回傳值]
	C --> |否|E
```

先抽象出一個介面接口，提供以下功能

- PeerPicker：server選擇遠端節點
- PeerGetter：client從group取得緩存資料

```go
// PeerGetter is an interface that be implemented by a peer/client
type PeerGetter interface {
	Get(group string, ket string) ([]byte, error)
}

// PeerPicker is an interface that be implemented to locate the peer that has a specific key
type PeerPicker interface {
	PickPeer(key string) (peer PeerGetter, ok bool)
}
```

並在`Part3 Server`部分，為`HttpPool`結構體加入`節點選擇`功能。 並具備特定key創建http client從遠端節點獲取緩存值的能力。

```go
// HttpPool implements PeerPicker for a pool of HTTP pools
type HttpPool struct {
	self       string // self address
	basePath   string
	mu         sync.Mutex // guards peers and httpGetters
	peers      *consistenthash.Map //一致性hash的map
	httpGetter map[string]*httpGetter // 映射遠端節點與對應的httpGetter
}
```

新增下面方法到框架主體中

- `RegisterPeers`：實現`PeerPicker`接口的`HttpPool`結構體放到`Group`中
- `getFromPeer`：實現`PeerGetter`接口的`Get`方法從遠端節點獲取緩存值
- 調整`load`方法，使用`getFromPeer`方法拿去遠端節點，若無，則調用`getLocally`

此部分

- 註冊節點(Register Peer)，使用一致性hash算法選擇節點
- 實現http client，與遠端節點的server溝通
  - 實現`peerPicker`
  - 實現`peerGetter`

測試如下

- 併發3個請求，從log可以看到，三次皆選擇了`8002`節點，這便體現了一致性hash的功用

```go
$ curl http://localhost:7000/api\?key\=Tom
630%                                                                                                                                                             $ curl http://localhost:7000/api\?key\=io 
io not exist

---- logs
2022/07/12 11:48:10 cache server is running at  
http://localhost:8001
2022/07/12 11:48:10 cache server is running at  http://localhost:8000
2022/07/12 11:48:10 cache server is running at  http://localhost:8002
2022/07/12 11:48:10 api server is running at http://localhost:7000
>>> start test
2022/07/12 11:48:12 [Server http://localhost:8002] Pick peer: http://localhost:8000
2022/07/12 11:48:12 [Server http://localhost:8002] Pick peer: http://localhost:8000
2022/07/12 11:48:12 [Server http://localhost:8002] Pick peer: http://localhost:8000
2022/07/12 11:48:12 [Server http://localhost:8000] GET /_cache/score/Tom
2022/07/12 11:48:12 [testDB] search key Tom
2022/07/12 11:48:12 [Server http://localhost:8000] GET /_cache/score/Tom
2022/07/12 11:48:12 [Cache] hit
2022/07/12 11:48:12 [Server http://localhost:8000] GET /_cache/score/Tom
2022/07/12 11:48:12 [Cache] hit
```

### 注意事項

- 確保類型實現了這些接口，如果沒有實現有可能會有問題

```go
var _ PeerGetter = (*httpGetter)(nil)
var _ PeerPicker = (*peerPicker)(nil)
```

## Part 6 - SingleFlight

前面測試部分，併發了3個請求，皆選擇了同一個節點，但如果有更多併發請求同一個數據時候，如果節點又同時向DB發起相同數量的請求，這有可能會導致`緩存擊穿`。

簡述常見緩存問題：

- `緩存擊穿`：一個key，在緩存過期時候，同時有大量請求，因為緩存失效，所以這些請求皆會直接訪問到DB，造成瞬間DB請求量爆增
- `緩存雪崩`：節點緩存同時間全部失效，大量請求瞬間造成DB壓力爆增，引起雪崩
- `緩存穿透`：查詢一個不存在的值，緩存及DB都沒有該值，如果瞬間流量過大，穿透DB，可能會導致DB當機

```go
type call struct {
  // avoid to visit peer with duplicate request
	wg  sync.WaitGroup 
	val interface{}
	err error
}

type Group struct {
	mu sync.Mutex       // protect m
  m  map[string]*call // concurrent caller (併發請求)
}
```

情境簡述為

瞬間大量請求`Get(key)`，但此key沒有被緩存過，此時沒有使用singleflight，那這些請求會發送到DB或是其他分佈式節點，導致其壓力爆增。使用singleflight，第一個請求到來，singleflight會紀錄當前的key被處理，其他的併發請求會被阻塞等待一個請求完成，一起取返回值即可

此部分

- 實現singleflight，防止緩存擊穿
  - 多併發相同請求，只發送一個請求給節點

測試如下

```go
$ curl http://localhost:7000/api\?key\=Tom
630%
$ curl http://localhost:7000/api\?key\=Jack
589%
---logs
2022/07/13 17:47:40 cache server is running at  http://localhost:8000
2022/07/13 17:47:40 cache server is running at  http://localhost:8002
2022/07/13 17:47:40 cache server is running at  http://localhost:8001
2022/07/13 17:47:40 api server is running at http://localhost:7000
>>> start test
//這個在前一部分顯示3筆，代表有3個請求
//此部分我們對於相同目標的併發請求，只發送一筆
2022/07/13 17:47:41 [Server http://localhost:8002] Pick peer: http://localhost:8000
2022/07/13 17:47:41 [Server http://localhost:8000] GET /_cache/score/Tom
2022/07/13 17:47:41 [testDB] search key Tom
6306306302022/07/13 17:47:59 [Server http://localhost:8002] Pick peer: http://localhost:8000
2022/07/13 17:47:59 [Server http://localhost:8000] GET /_cache/score/Tom
2022/07/13 17:47:59 [Cache] hit
2022/07/13 17:48:05 [Server http://localhost:8002] Pick peer: http://localhost:8001
2022/07/13 17:48:05 [Server http://localhost:8001] GET /_cache/score/Jack
2022/07/13 17:48:05 [testDB] search key Jack
```

