package main

import (
	"fmt"
	"geecache"
	"log"
	"net/http"
)

/*
$ curl http://localhost:8080/_cache/scor
bad request
$ curl http://localhost:8080/_cache/score/3
3 not exist
$ curl http://localhost:8080/_cache/score/Tom
630

--- logs
2022/07/08 17:31:10 [Server :8080] GET /_cache/scor
2022/07/08 17:31:13 [Server :8080] GET /_cache/score/3
2022/07/08 17:31:13 [TestDB] search key:  3
2022/07/08 17:31:23 [Server :8080] GET /_cache/score/Tom
2022/07/08 17:31:23 [TestDB] search key:  Tom

*/

var db = map[string]string{
	"Tom":  "630",
	"Jack": "589",
	"Sam":  "567",
}

func main() {
	geecache.NewGroup(
		"score",
		2<<10,
		// user decides how to use data from cache
		geecache.GetterFunc(func(key string) ([]byte, error) {
			log.Println("[TestDB] search key: ", key)
			if v, ok := db[key]; ok {
				return []byte(v), nil
			}
			return nil, fmt.Errorf("%s not exist", key)
		},
		),
	)
	addr := ":8080"
	peers := geecache.NewHttpPool(addr)
	log.Println("Cache is running at", addr)
	log.Fatal(http.ListenAndServe(addr, peers))

}

//// ServeHTTP is implemented by an object, we call that is a http handler
//func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
//	log.Println(r.URL.Path)
//	w.Write([]byte("ServeHTTP "))
//}
