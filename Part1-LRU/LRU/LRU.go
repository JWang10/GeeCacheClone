package LRU

import "container/list"

type Value interface {
	Len() int
}

type entry struct {
	key   string
	value Value
}

// Cache is an lru1 cache. It is not safe for concurrent access
type Cache struct {
	maxBytes int64
	nBytes   int64 // current cache used
	ll       *list.List
	cache    map[string]*list.Element
	// optional and executed when an entry is purged. (callback)
	OnEvicted func(key string, value Value)
}

// New is the constructor of Cache
func New(maxBytes int64, OnEvicted func(string, Value)) *Cache {
	return &Cache{
		maxBytes:  maxBytes,
		nBytes:    0,
		ll:        list.New(),
		cache:     make(map[string]*list.Element),
		OnEvicted: OnEvicted,
	}
}

// Get to find the value of key
func (c *Cache) Get(key string) (value Value, ok bool) {
	if node, ok := c.cache[key]; ok {
		c.ll.MoveToFront(node) // 因是雙向鏈表，隊首隊尾是相對的，此處當作隊尾
		kv := node.Value.(*entry)
		return kv.value, true
	}
	return
}

//RemoveOldest removes the oldest item
func (c *Cache) RemoveOldest() {
	node := c.ll.Back() // 照上面說明，所以此處Back代表隊首
	if node != nil {
		c.ll.Remove(node)
		kv := node.Value.(*entry)
		delete(c.cache, kv.key)
		c.nBytes -= int64(len(kv.key)) + int64(kv.value.Len()) // 更新當前內存
		if c.OnEvicted != nil {
			c.OnEvicted(kv.key, kv.value)
		}
	}
}

// Add a value to the cache
func (c *Cache) Add(key string, value Value) {
	if node, ok := c.cache[key]; ok {
		c.ll.MoveToFront(node)
		kv := node.Value.(*entry)
		c.nBytes += int64(value.Len()) - int64(kv.value.Len())
		kv.value = value
	} else {
		node := c.ll.PushFront(&entry{ // 如果已經存在，更新緩存節點
			key:   key,
			value: value,
		})
		c.cache[key] = node
		c.nBytes += int64(len(key)) + int64(value.Len())
	}
	for c.maxBytes != 0 && c.maxBytes < c.nBytes { // 緩存滿了，淘汰最近最少使用的數據
		c.RemoveOldest()
	}
}

// Len the number of cache entries
func (c *Cache) Len() int {
	return c.ll.Len()
}
